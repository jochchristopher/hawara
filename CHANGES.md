# 1.0.0 (initial release)

## Added

* H (INITIALIZER)

* Hawara (CLASS)

* select (METHOD)
* qs (METHOD)
* addClass (METHOD)
* removeClass (METHOD)
* html (METHOD)
* attr (METHOD)
* text (METHOD)
* content (METHOD)
* get (METHOD)
* create (METHOD)
* appendTo (METHOD)
* append (METHOD)

## Removed

---
