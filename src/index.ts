class Hawara {
    private document: Document = document;
    private element: HTMLElement | null = null;
    private elements: HTMLElement[] = [];
    constructor(d?: HTMLElement) {
        this.element = d || null;
    }

    /**
     * Method that select an Element and allows Method chaining.
     */
    select<T extends HTMLElement>(selector: string, parent?: HTMLElement) {
        if (parent) {
            this.element = <T | null>parent.querySelector(selector);
        } else {
            this.element = <T | null>this.document.querySelector(selector);
        }
        return this;
    }

    /**
     * Method that is like querySelector but just with a shorter name
     */
    qs<T extends HTMLElement>(selectors: string, parent?: HTMLElement) {
        if (parent) {
            return <T | null>parent.querySelector(selectors);
        } else {
            return <T | null>this.document.querySelector(selectors);
        }
    }

    /**
     * Method to add one or more classes to the current Element.
     */
    addClass(...tokens: string[]) {
        this.element?.classList.add(...tokens);
        return this;
    }

    /**
     * Method to remove one or more classes from the current Element.
     */
    removeClass(...tokens: string[]) {
        this.element?.classList.remove(...tokens);
        return this;
    }

    /**
     * Method that sets or returns the innerHTML of the current Element.
     */
    html(content?: undefined): string | null;
    html(content: any): this
    html(content: any) {
        if (content === undefined) {
            if (this.element) {
                return this.element.innerHTML
            } else {
                return null;
            }
        } else {
            if (this.element) {
                this.element.innerHTML = content;
            }
            return this
        }
    }

    /**
     * Method that sets or returns the the specified attr or all attributes of the current Element
     */
    attr(name: string, value?: undefined): string | null
    attr(name: string, value: string | number): this
    attr(name?: undefined, value?: undefined): this
    attr(name: any, value: any) {
        if (name) {
            if (value) {
                this.element?.setAttribute(name, value);
                return this;
            } else {
                return this.element?.getAttribute(name);
            }
        } else {
            return this;
        }
    }

    /**
     * Method that sets or returns the innerText of the current Element.
     */
    text(content?: undefined): string | null;
    text(content: any): this
    text(content: any) {
        if (content === undefined) {
            if (this.element) {
                return this.element.innerText
            } else {
                return null;
            }
        } else {
            if (this.element) {
                this.element.innerText = content;
            }
            return this
        }
    }

    /**
     * Method that sets or returns the textContent of the current Element.
     */
    content(content?: undefined): string | null;
    content(content: any): this
    content(content: any) {
        if (content === undefined) {
            if (this.element) {
                return this.element.textContent
            } else {
                return null;
            }
        } else {
            if (this.element) {
                this.element.textContent = content;
            }
            return this
        }
    }

    /**
     * Method that returns current Element. Keep in mind that you will probably need to tell your ide which type it is, because its type will be HTMLElement or null. 
     */
    get<T extends HTMLElement>() {
        return <T>this.element
    }

    /**
     * Method that creates an Element and either returns it or saves it and returns the instance
     */
    create<S extends keyof HTMLElementTagNameMap>(tageName: S, re: true): HTMLElementTagNameMap[S]
    create<S extends keyof HTMLElementTagNameMap>(tageName: S, re?: false): this
    create(tagName: any, re: any) {
        if (re) {
            return this.document.createElement(tagName);
        }
        this.element = this.document.createElement(tagName);
        return this;
    }

    /**
     * Method that appends current Element to DOM. Either to a given Parent or by default to the body
     */
    appendTo(parent?: undefined): this
    appendTo(parent: HTMLElement): this
    appendTo(parent: any) {
        if (parent instanceof HTMLElement) {
            if (this.element) {
                parent.appendChild(this.element);
                return this;
            } else {
                return this;
            }
        } else {
            if (this.element) {
                this.document.body.appendChild(this.element);
                return this;
            } else {
                return this;
            }
        }
    }

    /**
     * Method that appends given Elements to the current Element. If no Element is given nothing happens
     */
    append(...elements: HTMLElement[]) {

    }

    /**
     * Method that returns the dataset of the current element.
     */
    data() {
        return this.element?.dataset;
    }
}

/**
 * Main Function. Use it for enabling the power of this library.
 * If no selector or element is given then it will select null.
 * So if you want to create Elements for example, then no argument is the best choice.
 */
function H(value: string): Hawara
function H(value?: undefined): Hawara
function H(value: HTMLElement): Hawara
function H(value: any) {
    if (typeof value === 'string') { // dom
        return new Hawara().select(value);
    } else if (value) {
        if (value instanceof HTMLElement)
            return new Hawara(value);
    } else {
        return new Hawara();
    }
};