# Hawara

## Note: This library is for my personal usage, don't expect the next jQuery :)

## Hawara making DOM interaction easy

---

## What is Hawara?

Hawara is a fast and small JavaScript / Typescript library. It's API Methods are similiar to the native ones making it easy to learn and use.

---

## Core Features

* Element Creation
* Element Selection
* Method Chaining

---

## Why Hawara?

It was created due to my own laziness and because I did not like the way I had to structure my Code, especially when creating Elements. The idea was to create a library that is not there for replacing the native methods, but to make them easier to use. So using it feels natural.

## A Brief Look

### Selection of Elements

```javascript
H("body") // equal to document.body
H("button.btn") // selects the first button with the btn class

// it is also possible to use an already existing element

// assuming the btn is an HTMLButtonElement
H(btn) // selects the given button, no matter if it is in the dom or not

// if no argument is passed it will select null
H() // selects null -> perfect for creating elements
```

### Creation

```javascript
H().create('a') // creating <a> element
```

### Method Chaining

```javascript
// demonstrating with a simple example
H().create("a").addClass("link", "dark").text("Homepage").attr("href", "index.html").appendTo(H("body").get())
```

This Chain creates following:

```html
<body>
    <a class="link dark" href="index.html">Homepage</a>
</body>
```

### Side Note

```javascript
// one of the essential methods is get()
// it will return the current element
H('body').get() // will return the body element
```

In case you want to tell the ide what type

```typescript
// using typescripts implicit generic type
H('body').get<HTMLBodyElement>() // now the ide knows
```
